package com.zuitt.wdc044.services;

import com.zuitt.wdc044.models.Post;
import org.springframework.http.ResponseEntity;

import java.util.List;

public interface PostService {
    //create a post
    void createPost(String stringToken, Post post);

    //getting all posts
    Iterable<Post> getPosts();
    //get user posts
    Iterable<Post> getMyPosts(String stringToken);

    // edit a users post
    ResponseEntity updatePost(Long id, String stringToken, Post post);

    // Delete a users post
    ResponseEntity deletePost(Long id, String stringToken);
}
